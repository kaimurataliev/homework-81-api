const express = require('express');
const path = require('path');
const nanoid = require('nanoid');
const Links = require('../models/Links');

const config = require('../config');

const router = express.Router();

const createRouter = () => {

    router.post('/links', (req, res) => {
        const linkData = req.body;

        linkData.shortUrl = nanoid(7);
        const link = new Links(linkData);

        link.save()
            .then(result => res.send("http://localhost:8000/" + result.shortUrl))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:url', (req, res) => {
        const url = req.params.url;
        Links.findOne({shortUrl: url}, (error, result) => {
            try {
                res.status(301).redirect(result.originalUrl)
            } catch (error) {
                res.status(404).send(error)
            }
        })
    });

    return router;
};

module.exports = createRouter;