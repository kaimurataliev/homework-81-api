const mongoose = require('mongoose');

const LinkSchema = new mongoose.Schema({
    shortUrl: {
        type: String, required: true, unique: true
    },
    originalUrl: {
        type: String, required: true
    }
});

const Links = mongoose.model('Links', LinkSchema);

module.exports = Links;
